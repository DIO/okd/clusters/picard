# Sealed-Secrets

Gestion des secrets

## Prerequis

Par défaut, l’opérateur va créer un certificat utilisé pour chiffrer les secrets avec une durée limitée. Une fois la validité atteinte, l’opérateur va recréer un certificat automatiquement, mais il faut régénérer tous les sealed-secrets afin d'utiliser le nouveau certificat. Pour éviter ces manipulations, on peut créer un certificat avec une longue durée : 

Création d'un certificat utilisé par sealed secret avec une durée de 10 ans : 
```
openssl req -days 3650 -x509 -nodes -newkey rsa:4096 -keyout "tls.key" -out "tls.crt" -subj "/CN=sealed-secret/O=sealed-secret"
```

Une fois ce certificat créé, il faut le déployer dans un secret nommé "sealed-secrets-key" sur openshift. Nous pouvons créer un secret avec le script suivant : 
```
cat > sealed-secrets.yaml << EOF
type: kubernetes.io/tls
apiVersion: v1
kind: Secret
metadata:
  generateName: sealed-secrets-key
  labels:
    sealedsecrets.bitnami.com/sealed-secrets-key: active
  name: sealed-secrets-key
  namespace: kube-system
data:
EOF
crt=`cat tls.crt|base64 -w 0`
key=`cat tls.key|base64 -w 0`
echo "  tls.crt: $crt" >> sealed-secrets.yaml
echo "  tls.key: $key" >> sealed-secrets.yaml
```

puis le déployer avec la commande 

```
oc create -f sealed-secrets.yaml -n kube-system
```


## Installation

On utilise le namespace kube-system car la CLI de sealed-secrets (kubeseal) utilise ce namespace par défaut.


```
kustomize build .  | oc create -f -
```

### Installation de la CLI

```
wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.16.0/kubeseal-linux-amd64 -O kubeseal
sudo install -m 755 kubeseal /usr/local/bin/kubeseal
```

## Utilisation 

On génère un secret pour l'exemple : 
```
oc create secret generic secret-example --from-literal=password=mot_de_passe_exemple! --dry-run -o yaml > secret-example.yaml
```

On chiffre le secret : 
```
kubeseal < secret-example.yaml > sealed-secret-example.yaml
```

## Pour chiffrer un token (celui de cerbere par exemple)

Une fois le token mis dans le fichier keycloak-token.txt on créer le yaml avec 
```
oc create secret generic cerbere-clientsecret --from-file=clientSecret=keycloak-token.txt --dry-run=client -o yaml > tmp-secret.yaml
```
on édite ensuite le fichier tmp-secret.yaml pour rajouter le namespace et on chiffre avec

```
kubeseal -o yaml -n NAMESPACE < tmp-secret.yaml > cerbere-clientsecret.yaml
```


## Pour chiffrer un couple clef privé et certificat

On créer le yaml avec

```
oc create secret tls picard-certificates --cert=tls.crt --key=tls.key --dry-run=client -o yaml > tmp-secret.yaml
```

ensuite il faut éditer le fichier tmp-secret.yaml, rajouter le namespace et faire 

```
kubeseal -o yaml -n NAMESPACE < tmp-secret.yaml > picard-certificates.yaml
```


### Attention !

Le chiffrement du secret prend en compte le namespace et le nom du secret. Modifier un sealedsecret (par exemple le namespace du secret à générer) ne fonctionnera pas car le contrôleur n'arrivera pas à déchiffrer le sealedsecret ! 


