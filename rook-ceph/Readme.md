# Installer

Il est *indispensable* de faire les choses dans l'ordre décrit ci-dessous

  Installation d'abord de l'object security 
  
    oc create -f security/security.yaml
  
  Installation de l'operator
  
    cd operator
    helm dep update
    helm dep build
    helm install rook-ceph . 

  Installation du cluster

    cd cluster
    helm dep update
    helm dep build
    helm install rook-ceph-cluster . 

La console est sur 

  https://ceph-dashboard.apps.paas.obspm.fr

le mot de passe se récupère via 

  oc get secrets/rook-ceph-dashboard-password -o yaml |grep password: |awk '{print $2}' |base64 -d

# Déinstaller

Si ça merde et qu'il faut recommencer il faut tout supprimer avec

  helm uninstall rook-ceph-cluster
  helm uninstall rook-ceph
  oc -n rook-ceph patch cephcluster picard --type merge -p '{"spec":{"cleanupPolicy":{"confirmation":"yes-really-destroy-data"}}}'
  oc -n rook-ceph delete cephcluster picard

  for CRD in $(oc get crd -n rook-ceph | awk '/ceph.rook.io/ {print $1}'); do
   oc get -n rook-ceph "$CRD" -o name | xargs -I {} oc patch -n rook-ceph {} --type merge -p '{"metadata":{"finalizers": []}}'
  done

  oc -n rook-ceph patch configmap rook-ceph-mon-endpoints --type merge -p '{"metadata":{"finalizers": []}}'
  oc -n rook-ceph patch secrets rook-ceph-mon --type merge -p '{"metadata":{"finalizers": []}}'

  oc delete crds objectbucketclaims.objectbucket.io objectbuckets.objectbucket.io
  oc get crds --all-namespaces |grep -i ceph| awk '{printf "oc delete crds %s\n",$1}'|sh

  oc delete namespace rook-ceph

vérifiez que oc get namespaces ne retourne plus rook-ceph

Supprimer sur tous les computes :

  for i in `cat liste`; do echo $i; ssh core@$i.paas-m.prive 'sudo rm -rf  /var/lib/ceph /var/log/ceph /var/lib/rook'; done
