Pour installer le gitlab-runner il faut d'abord créer le namespace gitlab-runner
    
    oc create namespace gitlab
    oc project gitlab

Ensuite on utilise helm pour faire l'installation

    helm dep list
    helm dep build
    helm install gitlab-runner . --namespace gitlab

Il faut ensuite surveiller via 

    oc get all

que les images sont bien construites ce qui se voit par 

NAME                                                                           IMAGE REPOSITORY                                                                                        TAGS      UPDATED
imagestream.image.openshift.io/gitlab-runner-ocp-gitlab-runner                 image-registry.openshift-image-registry.svc:5000/gitlab/gitlab-runner-ocp-gitlab-runner                 v15.9.1   2 hours ago
imagestream.image.openshift.io/gitlab-runner-ocp-gitlab-runner-gitlab-helper   image-registry.openshift-image-registry.svc:5000/gitlab/gitlab-runner-ocp-gitlab-runner-gitlab-helper   v15.9.1   2 hours ago

On voit ici que les images sont disponibles dans le registry avec les version
(ici v15.9.1)

Ensuite (partie un peu tordu) il faut détruire le pod qui est censé tourner le
gitlab-runner (qui est en erreur) via 

    oc delete pod/gitlab-runner-ocp-gitlab-runner-XXXXX

*et* détruire le deployment avec 
    
    oc delete deployment.apps/gitlab-runner-ocp-gitlab-runner

puis redéployer via un 
    
    helm upgrade gitlab-runner . --namespace gitlab

La raison de ces dernières étapes est que lors du déployment OKD va chercher
l'image docker dont le nom est indiqué dans la config
(gitlab-runner-ocp-gitlab-runner) mais il ne le trouve pas (car l'image n'est
pas encore prête) donc il va essayer d'aller le chercher sur docker.io comme il
ne le trouve pas. Ça plante. 

Une fois détruite et recréer cela va fontionner

    
