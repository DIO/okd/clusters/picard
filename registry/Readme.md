On créer d'abord le volumeclaim en faisant :

    oc create -f volumes-registry.yaml

qui va créer le volume ceph

Ensuite il faut faire 

    oc patch config.image/cluster -p '{"spec":{"managementState":"Managed","replicas":2,"storage":{"managementState":"Unmanaged","pvc":{"claim":"registry"}}}}' --type=merge
  
pour dire à okd où se trouve le pvc pour le registry
