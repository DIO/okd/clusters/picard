# Description des fichiers

├── base
│   ├── argocd-dex-server-sa-sealedsecret.yaml
│   ├── argocd-sealed-secret.yaml

    La version chiffrée du token utiliser pour l'oauth 2 via l'authentification de okd
    voir le argocd-dex-server-sa.yaml où on créer le token pour que cela soit reproductible


│   └── argocd-ui-route.yaml

      Création d'une route qui va rediriger le traffic entrant sur le proxy vers la bonne
      machine

├── kustomization.yaml

      Fichier principale pour kustomize

└── overlays
    ├── argocd-application-controller.yaml
    ├── argocd-dex-server.yaml
      
      Surcharge pour sélectionner les bonnes VM (nodeSelector)

    ├── argocd-redis-ha-haproxy.yaml
    ├── argocd-redis.yaml

      Surcharge pour sélectionner les bonnes VM (nodeSelector)
      suppression des securityContext (qui ne fonctionnent pas sous okd)

    ├── argocd-cm.yaml

      Configuration d'une configmap pour l'authentification pour l'authentification oauth
      
    ├── argocd-dex-server-sa.yaml

      Création d'un «ServiceAccount»

          https://docs.okd.io/4.10/authentication/using-service-accounts-as-oauth-client.html

    ├── argocd-rbac-cm.yaml

      Définition des droits

    ├── argocd-repo-server-deploy.yaml
    └── argocd-server-deploy.yaml

      Définition des options de lancement pour argocd-server


